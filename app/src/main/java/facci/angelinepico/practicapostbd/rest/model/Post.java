package facci.angelinepico.practicapostbd.rest.model;

import com.google.gson.annotations.SerializedName;

public class Post {
    @SerializedName("title")
    private String Title;
    @SerializedName("descripcion")
    private String Descripcion;
    @SerializedName("Url_image")
    private String UrlImage;

    public Post(String title, String descripcion, String urlImage) {
        Title = title;
        Descripcion = descripcion;
        UrlImage = urlImage;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getUrlImage() {
        return UrlImage;
    }

    public void setUrlImage(String urlImage) {
        UrlImage = urlImage;
    }
}
