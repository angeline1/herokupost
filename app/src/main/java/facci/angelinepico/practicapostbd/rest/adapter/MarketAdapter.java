package facci.angelinepico.practicapostbd.rest.adapter;


import java.util.List;

import facci.angelinepico.practicapostbd.rest.constants.ApiConstants;
import facci.angelinepico.practicapostbd.rest.model.Post;
import facci.angelinepico.practicapostbd.rest.service.MarketService;
import retrofit2.Call;

public class MarketAdapter extends BaseAdapter implements MarketService {

    private MarketService marketService;

    //constructor
    public MarketAdapter(){
        super(ApiConstants.BASE_POST_URL);

        //inicio del servicio retrofit
        marketService = createService(MarketService.class);
    }
    //Metodo sobre escritos de la interface Market Service
    @Override
    public Call<Post> InsertPost(Post post){
        return marketService.InsertPost(post);
    }

    @Override
    public Call<List<Post>> getPosts(){
        return marketService.getPosts();
    }

}