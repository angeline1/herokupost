package facci.angelinepico.practicapostbd;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import facci.angelinepico.practicapostbd.rest.adapter.MarketAdapter;
import facci.angelinepico.practicapostbd.rest.model.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
 public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText EditTexttitle, EditTextDescripcion;
    private Button Buttonpost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniViews();
        Buttonpost.setOnClickListener(this);
    }

    private void iniViews() {
        EditTexttitle = findViewById(R.id.EditTextTitle);
        EditTextDescripcion = findViewById(R.id.EditTextDescripcion);
        Buttonpost = findViewById(R.id.ButtonPost);
    }

    private void PostPost() {
        MarketAdapter adapter = new MarketAdapter();
        Call<Post> call = adapter.InsertPost(
                new Post(EditTexttitle.getText().toString(),
                        EditTextDescripcion.toString(), "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=2ahUKEwiD9IDAlJjnAhWjzVkKHahRBpgQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.eluniversohoy.net%2Fla-puesta-de-sol-desde-florida-estados-unidos-3%2F&psig=AOvVaw29hV-bSmkIezZaPuv8cnob&ust=1579815227139822"));
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Log.e("response", response.body().toString());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ButtonPost:
                PostPost();
                break;
        }

    }
}